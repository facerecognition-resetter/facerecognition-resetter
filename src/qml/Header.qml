/*
 * This file is part of facerecognition-resetter
 *
 * Copyright (C) 2012 Igalia S.L.
 *
 * Contact: Andres Gomez <agomez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */


import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0
import 'constants.js' as UIConstants

Item {
    height: headerRectangle.height
    width: parent.width

    Rectangle {
        id: headerRectangle
        width: parent.width
        height: appWindow.inPortrait ?
                    UIConstants.HEADER_DEFAULT_HEIGHT_PORTRAIT :
                    52
        color: '#8402b6'
    }

    Text {
        id: headerText
        anchors.top: parent.top
        anchors.topMargin: appWindow.inPortrait ?
                               UIConstants.HEADER_DEFAULT_TOP_SPACING_PORTRAIT :
                               UIConstants.HEADER_DEFAULT_TOP_SPACING_LANDSCAPE
        anchors.left: parent.left
        anchors.leftMargin: UIConstants.DEFAULT_MARGIN
        font.pixelSize: UIConstants.FONT_LARGE
        font.family: UIConstants.FONT_FAMILY
        text: 'Face Recognition Resetter'
        color: 'white'
    }
}
