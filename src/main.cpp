/*
 * This file is part of facerecognition-resetter
 *
 * Copyright (C) 2012 Igalia S.L.
 *
 * Contact: Andres Gomez <agomez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */


#include "controller.h"
#include <QApplication>
#include <QDeclarativeContext>
#include <QDeclarativeView>
#include <QGraphicsObject>
#ifndef QT_SIMULATOR
    #include <MDeclarativeCache>
#endif

Q_DECL_EXPORT int main(int argc, char *argv[])
{
#ifndef QT_SIMULATOR
    QApplication *app = MDeclarativeCache::qApplication(argc, argv);
    QDeclarativeView *view = MDeclarativeCache::qDeclarativeView();
#else
    QApplication *app = new QApplication(argc, argv);
    QDeclarativeView *view = new QDeclarativeView();
#endif

    view->setSource(QUrl("qrc:/qml/main.qml"));
    view->showFullScreen();

    QDeclarativeContext *context = view->rootContext();
    QGraphicsObject *graphicsObject = view->rootObject();
    Controller *controller = new Controller(context, graphicsObject);

    int result = app->exec();

    delete controller;
    delete view;
    delete app;

    return result;
}
