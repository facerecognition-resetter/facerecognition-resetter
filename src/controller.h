/*
 * This file is part of facerecognition-resetter
 *
 * Copyright (C) 2012 Igalia S.L.
 *
 * Contact: Andres Gomez <agomez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */


#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>

class QDeclarativeContext;
class QGraphicsObject;

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QDeclarativeContext *context,
                        QGraphicsObject* graphicsObject);

public Q_SLOTS:
    void dealWithDB(int dealType) const;

private:
    bool deleteDB(QString &infoText) const;
    bool protectDB(bool protect, QString &infoText) const;
    QDeclarativeContext *m_declarativeContext;
    QGraphicsObject     *m_rootGraphicsObject;
    QString m_packageVersion;
};

#endif // CONTROLLER_H
