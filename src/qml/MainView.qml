/*
 * This file is part of facerecognition-resetter
 *
 * Copyright (C) 2012 Igalia S.L.
 *
 * Contact: Andres Gomez <agomez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */


import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0
import 'constants.js' as UIConstants

Page {
    id: mainPage

    property int dealType

    dealType: -1


    Header { id: header }


    function dealWithDB() {
        controller.dealWithDB(dealType);

        infoBanner.show();
    }


    QueryDialog {
        id: confirmationDialog
        acceptButtonText : 'Accept'
        rejectButtonText : 'Reject'
        onAccepted: dealWithDB(mainPage.dealType);
    }


    InfoBanner {
        id: infoBanner
        objectName: "infoBanner"
        text: "This is an info banner with no icon"
    }


    function openDialog(dealType) {
        mainPage.dealType = dealType;

        if (dealType == 0) {
           confirmationDialog.titleText = 'Reset?';
           confirmationDialog.message = 'Are you sure you want to reset the DB?';
        }
        if (dealType == 1) {
           confirmationDialog.titleText = 'Protect?';
           confirmationDialog.message = 'Are you sure you want to protect the DB?';
        }
        if (dealType == 2) {
           confirmationDialog.titleText = 'Unprotect?';
           confirmationDialog.message = 'Are you sure you want to unprotect the DB?';
        }

        confirmationDialog.open();
    }


    Button {
        id: buttonDelete
        anchors.top: header.bottom
        width: parent.width
        text: "Reset Face Recognition Data Base"
        onClicked: openDialog(0)
    }


    Button {
        id: buttonProtect
        anchors.top: buttonDelete.bottom
        width: parent.width
        text: "Protect Face Recognition Data Base"
        onClicked: openDialog(1)
    }


    Button {
        id: buttonUnprotect
        anchors.top: buttonProtect.bottom
        width: parent.width
        text: "Unprotect Face Recognition Data Base"
        onClicked: openDialog(2)
    }


    Item {
        id: definitionContainer
        y: UIConstants.PADDING_XXLARGE
        x: UIConstants.PADDING_XXLARGE
        width: parent.width - 2 * UIConstants.PADDING_XXLARGE
        height: responseText.height + 2 * UIConstants.PADDING_XXLARGE
        anchors.top: buttonUnprotect.bottom

        Text {
             id: responseText
             objectName: "responseText"
             font.pixelSize: UIConstants.FONT_LSMALL
             font.family: UIConstants.FONT_FAMILY
             width: parent.width
             height: implicitHeight
             wrapMode: Text.WordWrap
             text: '<br/><br/><br/><br/>' +
                   'This is just a program for resetting ' +
                   'or un/protecting ' +
                   'the Facerecognition database.<br/><br/>' +
                   'Just click on the Reset, ' +
                   'Protect and/or Unprotect button/s.'
             horizontalAlignment: Text.AlignHCenter
             verticalAlignment: Text.AlignVCenter
        }
    }


    Component {
        id: aboutView
        AboutView { }
    }

    Menu {
        id: mainMenu
        MenuLayout {
            MenuItem {
                id: aboutEntry
                text: 'About'
                onClicked: appWindow.pageStack.push(aboutView)
            }
        }
    }
    tools: ToolBarLayout {
        id: commonTools

        ToolIcon {
            iconId: 'toolbar-view-menu'
            anchors.right: parent.right
            onClicked: (mainMenu.status == DialogStatus.Closed) ?
                           mainMenu.open() : mainMenu.close()
        }
    }
}
