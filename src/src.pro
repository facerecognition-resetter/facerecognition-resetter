! include( ../common.pri ) {
    error( "Couldn't find the common.pri file!" )
}

TEMPLATE = app
QT += declarative
TARGET = "facerecognition-resetter"
DEPENDPATH += .
INCLUDEPATH += .


# Relative path definitions for face recognition
DEFINES += GALLERYCORE_DATA_ROOT_DIR=\\\"$${GALLERYCORE_DATA_ROOT_DIR}\\\"
DEFINES += GALLERYCORE_DATA_DATA_DIR=\\\"$${GALLERYCORE_DATA_DATA_DIR}\\\"
DEFINES += FACE_RECOGNITION_DATABASE_FILENAME=\\\"$${FACE_RECOGNITION_DATABASE_FILENAME}\\\"
DEFINES += GALLERYCORE_USER=\\\"$${GALLERYCORE_USER}\\\"
DEFINES += GALLERYCORE_GROUP=\\\"$${GALLERYCORE_GROUP}\\\"
DEFINES += REGULAR_USER=\\\"$${REGULAR_USER}\\\"
DEFINES += REGULAR_GROUP=\\\"$${REGULAR_GROUP}\\\"

PACKAGEVERSION = $$system(head -n 1 ../debian/changelog | grep -o [0-9].[0-9].[0-9])
DEFINES += "PACKAGEVERSION=\\\"$$PACKAGEVERSION\\\""


# although the app doesn't use meegotouch by itself
# linking against it removes several style warnings
CONFIG += meegotouch

# enable booster
CONFIG += qt-boostable qdeclarative-boostable

# booster flags
QMAKE_CXXFLAGS += -fPIC -fvisibility=hidden -fvisibility-inlines-hidden
QMAKE_LFLAGS += -pie -rdynamic

# Input
SOURCES += main.cpp \
    controller.cpp

HEADERS += \
    controller.h

OTHER_FILES += \
    qml/main.qml \
    qml/MainView.qml \
    qml/Header.qml \
    qml/AboutView.qml \
    qml/constants.js

RESOURCES += \
    res.qrc

unix {
    #VARIABLES
    isEmpty(PREFIX) {
        PREFIX = /opt/$${TARGET}
    }
    BINDIR = $$PREFIX/bin
    DATADIR =/usr/share
    PKGDATADIR =$$PREFIX/share

    DEFINES += DATADIR=\\\"$$DATADIR\\\" PKGDATADIR=\\\"$$PKGDATADIR\\\"

    #MAKE INSTALL

    INSTALLS += target desktop icon80 splash

    target.path =$$BINDIR

    desktop.path = $$DATADIR/applications
    desktop.files += $${TARGET}.desktop

    icon80.path = $$DATADIR/icons/hicolor/80x80/apps
    icon80.files += ../data/icon-l-$${TARGET}.png

    splash.path = $$PKGDATADIR/splash/
    splash.files += ../data/facerecognition-resetter-splash-landscape.jpg
    splash.files += ../data/facerecognition-resetter-splash-portrait.jpg
}

